**Parser**

Usage: 

`python gram_parser.py EXPRESSION GRAMMAR_1 GRAMMAR_2 ...`

Example (natural grammar): 

`python gram_parser.py ((2+2)*2-4/2-1+7*3-6)/4 S->S+T S->S-T S->T T->T*F T->T/F T->F F->(S) F->R`